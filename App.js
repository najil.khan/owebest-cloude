import 'react-native-gesture-handler';
import React from 'react'
import { UserProvider } from './src/Context/UserContext'
import RootNavigation from './src/Navigation/RootNavigation';

const App = () => {
  return (
    <UserProvider>
      <RootNavigation />
    </UserProvider>
  )
}

export default App