import React, { useContext, useState } from 'react';

export const UserContext = React.createContext({
  isAuth: false,
  updateIsAuth: () => { },
});
export const UserProvider = ({ children }) => {
  const [isAuth, setIsAuth] = useState(false);
  return (
    <UserContext.Provider
      value={{
        isAuth,
        updateIsAuth: setIsAuth,
      }}>
      {children}
    </UserContext.Provider>
  );
};

export const useAuth = () => {
  const { isAuth, updateIsAuth } = useContext(UserContext);
  return { isAuth, updateIsAuth };
};
