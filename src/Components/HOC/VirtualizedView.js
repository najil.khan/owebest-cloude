import React from 'react';
import { FlatList, ScrollView, StyleSheet } from 'react-native';
import colors from '../../Constants/colors';
import ViewContainer, { containerStyle } from './ViewContainer';

export default function VirtualizedView({
  children,
  refreshControl,
  contentContainerStyle,
  backgroundColor,
}) {
  return (
    <FlatList
      showsVerticalScrollIndicator={false}
      style={[containerStyle.container(backgroundColor), styles.scroll]}
      data={[]}
      refreshControl={refreshControl}
      ListEmptyComponent={null}
      keyExtractor={() => 'List'}
      renderItem={null}
      contentContainerStyle={contentContainerStyle}
      ListHeaderComponent={() => <React.Fragment>{children}</React.Fragment>}
    />
  );
}

const styles = StyleSheet.create({
  scroll: {},
});
