import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import LinearGradient from 'react-native-linear-gradient'
import colors from '../../Constants/colors';

const ColoredView = ({
    children,
    style,
    viewColors = [colors.themeA, colors.white, colors.themeB],
    start = { x: 2, y: -1 },
    end,
    locations
}) => {
    return (
        <LinearGradient colors={viewColors} style={style} start={start} end={end} locations={locations}>
            {children}
        </LinearGradient>
    )
}

export default ColoredView

const styles = StyleSheet.create({})