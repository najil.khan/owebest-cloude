import React, { Component } from 'react';
import { StyleSheet, View, Alert, Text } from 'react-native';

import {
  LongPressGestureHandler,
  ScrollView,
  State,
  TapGestureHandler,
  LongPressGestureHandlerStateChangeEvent,
  TapGestureHandlerStateChangeEvent,
} from 'react-native-gesture-handler';

// import { LoremIpsum } from '../../common';

interface PressBoxProps {
  setDuration?: (duration: number) => void;
}

interface ExampleState {
  longPressDuration: number;
}
export class PressBox extends Component<PressBoxProps> {
  private doubleTapRef = React.createRef<TapGestureHandler>();
  private onHandlerStateChange = (
    event: LongPressGestureHandlerStateChangeEvent
  ) => {
    this.props.setDuration?.(event.nativeEvent.duration);
  };
  private onSingleTap = (event: TapGestureHandlerStateChangeEvent) => {
    if (event.nativeEvent.state === State.ACTIVE) {
      Alert.alert("I'm touched");
    }
  };
  private onDoubleTap = (event: TapGestureHandlerStateChangeEvent) => {
    if (event.nativeEvent.state === State.ACTIVE) {
      Alert.alert('triple tap, good job!');
    }
  };
  render() {
    return (
      <LongPressGestureHandler
        onHandlerStateChange={this.onHandlerStateChange}
        minDurationMs={800}>
        <TapGestureHandler
          // onHandlerStateChange={this.onSingleTap}
          waitFor={this.doubleTapRef}>
          <TapGestureHandler
            ref={this.doubleTapRef}
            onHandlerStateChange={this.onDoubleTap}
            numberOfTaps={3}>
            <View style={styles.box} />
          </TapGestureHandler>
        </TapGestureHandler>
      </LongPressGestureHandler>
    );
  }
}

export default class Login extends Component<
  Record<string, never>,
  ExampleState
> {
  constructor(props: Record<string, never>) {
    super(props);

    this.state = { longPressDuration: 0 };
  }

  render() {
    return (
      <ScrollView style={styles.scrollView}>
        <PressBox
          setDuration={(duration: number) =>
            this.setState({ longPressDuration: duration })
          }
        />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    flex: 1,
  },
  box: {
    width: 150,
    height: 150,
    alignSelf: 'center',
    backgroundColor: 'plum',
    margin: 10,
    zIndex: 200,
  },
  text: {
    marginLeft: 20,
  },
});
