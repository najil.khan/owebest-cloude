import {Platform, KeyboardAvoidingView, StyleSheet} from 'react-native';
import React from 'react';
import {containerStyle} from './ViewContainer';
import colors from '../../Constants/colors';
const keyboardVerticalOffset = Platform.OS === 'ios' ? 20 : 0;
const behavior = Platform.OS === 'ios' ? 'padding' : undefined
const FormContainer = ({
  children,
  backgroundColor = colors.bgcolor,
  style = {},
}) => {
  return (
    <KeyboardAvoidingView

      style={[containerStyle.container(backgroundColor), styles.keyboard]}
      behavior={behavior}
      keyboardVerticalOffset={keyboardVerticalOffset}>
        {children}
    </KeyboardAvoidingView>
  );
};

export default FormContainer;
const styles = StyleSheet.create({
  keyboard: {
  },
});
