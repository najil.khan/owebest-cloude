import React, { Component } from 'react';
import { StyleSheet, View, Alert } from 'react-native';

import {
  LongPressGestureHandler,
  ScrollView,
  State,
  TapGestureHandler,
  LongPressGestureHandlerStateChangeEvent,
  TapGestureHandlerStateChangeEvent,
} from 'react-native-gesture-handler';
import Typography from '../UI/Typography';

interface PressBoxProps {
  setDuration?: (duration: number) => void;
  title?:String;
  onTripleClick?: Function
  style?:any;
  titleSize?:any;
  titleColor?:any;
}
export default class Clickable extends Component<PressBoxProps> {
  private doubleTapRef = React.createRef<TapGestureHandler>();
  private onHandlerStateChange = (
    event: LongPressGestureHandlerStateChangeEvent
  ) => {
    this.props.setDuration?.(event.nativeEvent.duration);
  };
  private onSingleTap = (event: TapGestureHandlerStateChangeEvent) => {
    if (event.nativeEvent.state === State.ACTIVE) {
      // Alert.alert("I'm touched");
    }
  };
  private onTripleTap = (event: TapGestureHandlerStateChangeEvent) => {
    if (event.nativeEvent.state === State.ACTIVE) {
      // Alert.alert('Double tap, good job!');
      this.props.onTripleClick?.()
    }
  };
  render() {
    return (
      <LongPressGestureHandler
        onHandlerStateChange={this.onHandlerStateChange}
        minDurationMs={800}>
        <TapGestureHandler
          onHandlerStateChange={this.onSingleTap}
          waitFor={this.doubleTapRef}>
          <TapGestureHandler
            ref={this.doubleTapRef}
            onHandlerStateChange={this.onTripleTap}
            numberOfTaps={3}>
            <View style={[styles.box,this.props.style]}>
            <Typography size={this.props.titleSize}
             color={this.props.titleColor}
             >
              {this.props.title}
            </Typography>
            </View>
          </TapGestureHandler>
        </TapGestureHandler>
      </LongPressGestureHandler>
    );
  }
}

const styles = StyleSheet.create({
  box: {
    width: '90%',
    height: 50,
    alignSelf: 'center',
    backgroundColor: 'black',
    margin: 10,
    zIndex: 200,
    borderRadius:50
  },
});