import { StyleSheet, ScrollView, FlatList } from 'react-native';
import React from 'react';
import ViewContainer, { containerStyle } from './ViewContainer';
import colors from '../../Constants/colors';

const ScrollContainer = ({
  children,
  backgroundColor = colors.bgcolor,
  style = {},
  contentContainerStyle = { paddingBottom: 82 },
  refreshControl,
  stickyHeaderIndices,
  onScroll,
  useSafeAreaView = false,
}) => {
  return (
    <ViewContainer useSafeAreaView={useSafeAreaView} backgroundColor={backgroundColor} style={style}>
      <ScrollView
        onScroll={onScroll}
        contentContainerStyle={contentContainerStyle}
        refreshControl={refreshControl}
        showsVerticalScrollIndicator={false}
        stickyHeaderIndices={stickyHeaderIndices}
        style={[containerStyle.container(backgroundColor), styles.scroll]}>
        {children}
      </ScrollView>
    </ViewContainer>
  );
};

export default ScrollContainer;

const styles = StyleSheet.create({
  scroll: {},
});
