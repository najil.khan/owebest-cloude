import { StyleSheet, SafeAreaView, View } from 'react-native';
import React from 'react';
import Container from './Container';
import colors from '../../Constants/colors';

const ViewContainer = ({
  children,
  backgroundColor = colors.white,
  style = {},
  useSafeAreaView = false,
}) => {
  return (
    <Container>
      {useSafeAreaView ? (
        <SafeAreaView
          style={[
            { backgroundColor: 'red' },
            containerStyle.container(backgroundColor),
            style,
          ]}>
          {children}
        </SafeAreaView>
      ) : (
        <View
          style={[
            { backgroundColor: 'red' },
            containerStyle.container(backgroundColor),
            style,
          ]}>
          {children}
        </View>
      )}
    </Container>
  );
};

export default ViewContainer;

export const containerStyle = StyleSheet.create({
  container: backgroundColor => {
    return { flex: 1, backgroundColor };
  },
});
