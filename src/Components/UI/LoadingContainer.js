import { ScrollView, StyleSheet, Text, View } from 'react-native';
import React from 'react';
import Container from '../HOC/Container';
import Spinner from './Spinner';
import Typography from './Typography';
import colors from '../../Constants/colors';
import { FULL_HEIGHT, STANDARD_WIDTH } from '../../Constants/layout';
import VirtualizedView from '../HOC/VirtualizedView';
import globalStyle from '../../Constants/globalStyle';

const LoadingContainer = ({
  isLoading = false,
  data = [],
  children,
  emptyText = 'No Data',
  refreshControl,
}) => {
  return (
    <Container>
      {isLoading ? (
        <Spinner />
      ) : (
        <Container>
          {data.length ? (
            <ScrollView refreshControl={refreshControl}>{children}</ScrollView>
          ) : (
            <ScrollView
              refreshControl={refreshControl}
              contentContainerStyle={{ flex: 1, justifyContent: 'center' }}>
              <Typography
                size={35}
                color={colors.Orange}
                textAlign="center"
                type="medium">
                {emptyText}
              </Typography>
            </ScrollView>
          )}
        </Container>
      )}
    </Container>
  );
};

export default LoadingContainer;

const styles = StyleSheet.create({});
