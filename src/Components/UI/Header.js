import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { SPACING } from '../../Constants/layout'
import Typography from './Typography'
import colors from '../../Constants/colors'
import Icon from '../../Components/UI/Icon'

const Header = ({
    headerLeft,
    backgroundColor = colors.white,
    center,
    headerRight,
    title,
    titleColor = colors.black,
    leftContainerStyle,
    rightContainerStyle,
    centerContainerStyle,
    style
}) => {
    return (
        <View style={[styles.header, { backgroundColor: backgroundColor }, style]}>
            <View style={[leftContainerStyle, { justifyContent: 'center' }]}>
                {headerLeft}
            </View>
            <View style={{ flex: 1, paddingHorizontal: 8, justifyContent: 'center', ...centerContainerStyle }}>
                {center ? center :
                    <Typography size={20} color={titleColor}>
                        {title}
                    </Typography>
                }
            </View>
            <View style={[rightContainerStyle, { justifyContent: 'center' }]}>
                {headerRight}
            </View>
        </View>
    )
}

export default Header

const styles = StyleSheet.create({
    header: {
        width: '100%',
        paddingHorizontal: SPACING / 1.5,
        height: 55,
        justifyContent: 'center',
        flexDirection: 'row',
        elevation: 0.5
    }
})