import {StyleSheet, Text, View} from 'react-native';
import React, {memo} from 'react';
import colors from '../../Constants/colors';
import {STANDARD_WIDTH} from '../../Constants/layout';

const Devider = ({marginVertical = 0, height = 1, standardWidth}) => {
  return (
    <View style={styles.line(marginVertical, height, standardWidth)}></View>
  );
};

export default memo(Devider);

const styles = StyleSheet.create({
  line: (marginVertical, height, standardWidth) => {
    return {
      width: standardWidth ? STANDARD_WIDTH : '100%',
      height: height,
      marginVertical: marginVertical,
      backgroundColor: colors.Orange,
      borderRadius: 10,
      alignSelf: 'center',
    };
  },
});
