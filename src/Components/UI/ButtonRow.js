import {StyleSheet, View} from 'react-native';
import React from 'react';
import {STANDARD_WIDTH} from '../../Constants/layout';

const ButtonRow = ({children}) => {
  return <View style={styles.buttonRow}>{children}</View>;
};

export default ButtonRow;

const styles = StyleSheet.create({
  buttonRow: {
    flexDirection: 'row',
    width: STANDARD_WIDTH,
    justifyContent: 'space-between',
    alignItems: 'center',
    alignSelf: 'center',
    flexWrap: 'wrap',
  },
});
