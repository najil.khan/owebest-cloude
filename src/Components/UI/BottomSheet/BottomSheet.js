import {StyleSheet, Text, View, ActionSheetIOS} from 'react-native';
import React from 'react';
import BottomSheetItem from './BottomSheetItem';
import Reanimated, {SlideInDown} from 'react-native-reanimated';
import {FULL_WIDTH, SPACING} from '../../../Constants/layout';
const BottomSheet = ({visible = false, onClose = () => {}, actions = []}) => {
  return (
    <Reanimated.View entering={SlideInDown} style={styles.bottomSheet}>
      {actions.map(item => {
        return <BottomSheetItem title={item?.title} onPress={item?.onPress} />;
      })}
      <BottomSheetItem noBorder cancel title="Cancel" onPress={onClose} />
    </Reanimated.View>
  );
};

export default BottomSheet;

const styles = StyleSheet.create({
  bottomSheet: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    width: FULL_WIDTH,
    alignSelf: 'center',
    backgroundColor: '#fff',
    padding: SPACING,
    paddingVertical: SPACING / 2,
  },
  item: {},
});
