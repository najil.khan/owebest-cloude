import * as React from 'react';
import {
  Platform,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  View,
} from 'react-native';
import { useIsFocused } from '@react-navigation/native';

export default function FocusAwareStatusBar({ backgroundColor, ...props }) {
  const isFocused = useIsFocused();

  return (
    Platform.OS === 'ios' ? isFocused && <View style={[styles.statusBar, { backgroundColor }]}>
      <SafeAreaView>
        <StatusBar animated={true} backgroundColor={backgroundColor} {...props} />
      </SafeAreaView>
    </View>
      : isFocused ? <StatusBar backgroundColor={backgroundColor} {...props} /> : null
  );
}
const STATUSBAR_HEIGHT = StatusBar.currentHeight;
const styles = StyleSheet.create({
  statusBar: {
    height: STATUSBAR_HEIGHT,
  },
});
