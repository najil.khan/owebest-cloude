import {ActivityIndicator, Modal, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {FULL_WIDTH} from '../../Constants/layout';
import colors from '../../Constants/colors';

const LoadingModal = ({
  visible = false,
  onClose = () => {},
  animationType = 'fade',
  transparent = true,
  loaderanimating,
  loaderSize = 'large',
  loaderColor = colors.Orange,
  loaderStyle,
}) => {
  return (
    <Modal
      statusBarTranslucent
      animationType={animationType}
      transparent={transparent}
      visible={visible}
      onRequestClose={() => {
        onClose();
      }}>
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#00000099',
        }}>
        <View
          style={{
            width: FULL_WIDTH * 0.25,
            height: FULL_WIDTH * 0.25,
            borderRadius: 10,
            backgroundColor: colors.inputBgColor,
            elevation: 10,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator
            animating={loaderanimating}
            size={loaderSize}
            color={loaderColor}
            style={loaderStyle}
          />
        </View>
      </View>
    </Modal>
  );
};

export default LoadingModal;

const styles = StyleSheet.create({});
