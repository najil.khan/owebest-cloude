import {Animated, StyleSheet, Text, TextInput, View} from 'react-native';
import React, {memo} from 'react';
import colors from '../../Constants/colors';
import Svg, {Circle, G} from 'react-native-svg';

const AnimatedProgress = ({
  percentage = 70,
  size = 40,
  strokewidth = 6,
  duration = 500,
  color = colors.Orange,
  delay = 0,
  textColor = colors.Orange,
  max = 100,
  strokeLinecap = 'round',
  backgroundColor = colors.placeTextColor,
  showText = true,
  textSize,
  marginRight,
}) => {
  console.log('AnimatedProgress');
  const halfCircil = size + strokewidth;
  const circleCircumference = 2 * Math.PI * size;

  const activeCircleSize = 1;
  const AnimatedCiecle = Animated.createAnimatedComponent(Circle);
  const AnimatedInput = Animated.createAnimatedComponent(TextInput);

  const ciecleRef = React.useRef();
  const inputRef = React.useRef();
  const circleAnimation = val => {
    return Animated.timing(animatedValue, {
      toValue: val,
      duration: duration,
      delay: delay,
      useNativeDriver: true,
    }).start(() => {
      circleAnimation(percentage);
    });
  };
  const animatedValue = React.useRef(new Animated.Value(100)).current;
  React.useEffect(() => {
    circleAnimation(100);
    animatedValue.addListener(v => {
      if (ciecleRef?.current) {
        const maxPercentage = (100 * v.value) / max;
        const strokeDashoffset =
          circleCircumference - (circleCircumference * maxPercentage) / 100;
        ciecleRef.current.setNativeProps({
          strokeDashoffset,
        });
      }
      if (inputRef?.current) {
        inputRef.current.setNativeProps({
          text: `${Math.round(v.value)}%`,
        });
      }
    });
    return () => {
      animatedValue.removeAllListeners();
    };
  }, [max, percentage]);
  return (
    <View style={{marginRight: marginRight}}>
      <Svg
        width={size * 2}
        height={size * 2}
        viewBox={`0 0 ${halfCircil * 2} ${halfCircil * 2}`}>
        <G rotation={'-90'} origin={`${halfCircil},${halfCircil}`}>
          <Circle
            cx={'50%'}
            cy={'50%'}
            stroke={backgroundColor}
            strokeWidth={strokewidth}
            r={size}
            fill="transparent"
            strokeOpacity={0.5}
          />
          <AnimatedCiecle
            cx={'50%'}
            cy={'50%'}
            stroke={color}
            ref={ciecleRef}
            strokeWidth={strokewidth}
            r={size}
            fill="transparent"
            strokeDasharray={circleCircumference}
            strokeDashoffset={activeCircleSize}
            strokeLinecap={strokeLinecap}
          />
        </G>
      </Svg>
      {showText && (
        <AnimatedInput
          ref={inputRef}
          underlineColorAndroid={'transparent'}
          editable={false}
          defaultValue="0"
          style={[
            StyleSheet.absoluteFillObject,
            {
              fontSize: textSize ?? size / 2,
              color: textColor ?? color,
              textAlign: 'center',
              width: size * 2,
              fontWeight: 'bold',
            },
          ]}
        />
      )}
    </View>
  );
};

export default memo(AnimatedProgress);
