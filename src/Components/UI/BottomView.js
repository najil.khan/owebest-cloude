import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import colors from '../../Constants/colors';
import LinearGradient from 'react-native-linear-gradient';

const BottomView = ({StyleBottomView}) => {
  return (
    <LinearGradient
      start={{x: 1, y: 0}}
      end={{x: 1, y: 1}}
      colors={[colors.inputBgColor, colors.Orange, colors.inputBgColor]}
      style={styles.StyleBottomView}
    />
  );
};

export default BottomView;

const styles = StyleSheet.create({
  StyleBottomView: {
    width: '100%',
    height: 1,
  },
});
