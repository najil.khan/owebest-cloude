import { RefreshControl, StyleSheet, Text, View } from 'react-native'
import React from 'react'

const PageRefresher = ({
    colors=[colors.Orange],
    refreshing,
    onRefresh
}) => {
    return (
        <RefreshControl
            colors={colors}
            refreshing={refreshing}
            onRefresh={onRefresh}
        />
    )
}

export default PageRefresher

const styles = StyleSheet.create({})