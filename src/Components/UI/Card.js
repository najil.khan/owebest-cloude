import {StyleSheet, View} from 'react-native';
import React, {memo} from 'react';
import {SPACING, STANDARD_WIDTH} from '../../Constants/layout';

const Card = ({children, style = {}}) => {
  return <View style={[styles.card, style]}>{children}</View>;
};

export default memo(Card);

const styles = StyleSheet.create({
  card: {
    width: STANDARD_WIDTH,
    alignSelf: 'center',
    marginVertical: SPACING,
  },
});
