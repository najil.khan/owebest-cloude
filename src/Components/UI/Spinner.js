import {ActivityIndicator, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import colors from '../../Constants/colors';

const Spinner = ({
    animating, 
    color=colors.Orange, 
    size='large', 
    style,
    type = 'file'
}) => {
  return (
    <>
      {type=='button'?<ActivityIndicator
        animating={animating}
        size={size}
        color={color}
        style={style}
      />
      :<View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
      <ActivityIndicator
        animating={animating}
        size={size}
        color={color}
        style={style}
      />
    </View>}
    </>
    
  );
};

export default Spinner;

const styles = StyleSheet.create({});
