export const renderIf = (condition, Element = () => {}) => {
  if (condition !== null && condition !== undefined && condition !== '') {
    return Element;
  }
};

export const percentageCalculate = (val,total) => {
  return (val/total)*100
}
