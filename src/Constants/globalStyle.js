import {StyleSheet} from 'react-native';
import colors from './colors';
import {RADIUS} from './layout';
export default StyleSheet.create({
  button: {
    justifyContent: 'space-between',
    alignItems: 'center',
    alignSelf: 'center',
    height: 43,
    marginVertical: 5,
    marginHorizontal: 1,
    borderRadius: RADIUS,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,

    elevation: 6,
  },
  buttonDisable: {},
  noBorder: {
    borderBottomWidth: 0,
  },
  row:(margin)=>{
    return{
      flexDirection:'row',
      marginVertical:margin
    }
  },
  headerICon:{
    padding:10,
    backgroundColor:'#434343',
    borderRadius:10
  },
  commonPaddingBottom:{
    paddingBottom:80,
  }
});
