export default {
  white: '#ffffff',
  black: '#000000',
  red: 'red',
  textGray: '#767578',
  textLight: '#afadb5',
  theme: '#663ff5',
  themeA: '#3ec8c6',
  themeB: '#ff552e',
  inputBG: '#eeecf6'
};
