import { View, Text } from 'react-native'
import React from 'react'
import { CardStyleInterpolators, createStackNavigator } from '@react-navigation/stack'
const Stack = createStackNavigator()
const commonOptions = {
    headerShown: false,
    cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
}
export const AuthNavigator = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name='Login'
                getComponent={() => require('../screens/public/Login').default}
                options={{
                    ...commonOptions,
                }}
            />
        </Stack.Navigator>
    )
}
export const HomeStack = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name='Dashboard'
                getComponent={() => require('../screens/private/Dashboard').default}
                options={{
                    ...commonOptions,
                }}
            />
        </Stack.Navigator>
    )
}


