import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { useAuth } from '../Context/UserContext'
import DrawerNavigator from './DrawerNavigator'
import { AuthNavigator } from './StackNavigator'

const RootNavigation = () => {
  const { isAuth } = useAuth()
  return (
    <NavigationContainer>
      {
        !isAuth ?
          <DrawerNavigator /> :
          <AuthNavigator />
      }
    </NavigationContainer>
  )
}

export default RootNavigation

const styles = StyleSheet.create({})