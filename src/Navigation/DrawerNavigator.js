import { View, Text } from 'react-native'
import React from 'react'
import { createDrawerNavigator } from '@react-navigation/drawer'
import CustomDrawer from './CustomDrawer'
const Drawer = createDrawerNavigator()
const commonOptions = {
  headerShown: false
}
const DrawerNavigator = () => {
  return (
    <Drawer.Navigator
      drawerContent={(props) => {
        return <CustomDrawer
          {...props}
        />
      }}

    >
      <Drawer.Screen
        name='Dashboard'
        options={{
          ...commonOptions
        }}
        getComponent={() => require('../Navigation/BottomTabNavigator').default}
      />
    </Drawer.Navigator>
  )
}

export default DrawerNavigator