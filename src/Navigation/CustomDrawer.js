import { StyleSheet, Text, View } from 'react-native'
import React, { useState } from 'react'
import ViewContainer from '../Components/HOC/ViewContainer'
import colors from '../Constants/colors'
import Press from '../Components/HOC/Press'
import Typography from '../Components/UI/Typography'
import Icon from '../Components/UI/Icon'
import Icons from '../Constants/Icons'
import { FULL_HEIGHT } from '../Constants/layout'

const routeConfig = [
    {
        id: 0,
        name: 'Home',
        routeName: 'Dashboard',
        icon: Icons.homeActive
    },
    {
        id: 1,
        name: 'To Do',
        routeName: 'Todo',
        icon: Icons.todo
    },
]


const CustomDrawer = ({ navigation, ...props }) => {
    const [Background, setBackground] = useState(colors.theme)
    const onTabPress = (route, i) => {
        navigation.navigate(route)
    }
    console.log("🚀 ~ file: CustomDrawer.js ~ line 8 ~ CustomDrawer ~ props", props)
    return (
        <ViewContainer>
            <View style={styles.card}>
                <Icon
                    source={Icons.user}
                    size={80}
                />
                <Typography
                    size={15}
                    color={colors.theme}
                    style={{
                        fontWeight: 'bold',
                        marginTop: 10
                    }}
                >
                    Jhon Doe
                </Typography>
            </View>
            {
                routeConfig.map((item, i) => {
                    return (
                        <Press key={i} onPress={() => {
                            onTabPress(item.routeName, i)
                            // item.id === i ? setBackground(colors.theme) : setBackground(colors.white)
                        }} style={[styles.tab, { backgroundColor: Background }]}>
                            <Icon
                                source={item.icon}
                                tintColor={colors.white}
                                size={22}
                                marginRight={10}
                            />
                            <Typography color={colors.white} style={{ fontWeight: 'bold' }}>
                                {item.name}
                            </Typography>
                        </Press>
                    )
                })
            }
        </ViewContainer>
    )
}

export default CustomDrawer

const styles = StyleSheet.create({
    tab: {
        width: '95%',
        height: 50,
        borderRadius: 10,
        alignSelf: 'center',
        marginVertical: 5,
        padding: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    card: {
        width: '95%',
        height: FULL_HEIGHT * 0.25,
        borderRadius: 10,
        backgroundColor: colors.inputBG,
        alignSelf: 'center',
        marginVertical: 5,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    }
})