import { View, Text } from 'react-native'
import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from '../Components/UI/Icon';
import Icons from '../Constants/Icons';
import colors from '../Constants/colors';
import { SPACING } from '../Constants/layout';

const Tab = createBottomTabNavigator();
const commonOptions = {
    headerShown: false
}
const BottomTabNavigator = () => {
    return (
        <Tab.Navigator
            initialRouteName='Home'
            screenOptions={{
                ...commonOptions,
                tabBarStyle: {
                    height: 70,
                    position: 'absolute',
                    margin: 12,
                    marginHorizontal: SPACING - 5,
                    borderRadius: 18,
                    backgroundColor: colors.white,
                    borderTopWidth: 0
                },
                tabBarShowLabel: false,
            }}
        >
            <Tab.Screen
                name='Home'
                options={{
                    ...commonOptions,
                    tabBarIcon: ({ focused }) => <Icon
                        source={focused ? Icons.homeActive : Icons.homeInactive}
                        size={focused ? 30 : 25}
                        tintColor={focused ? colors.theme : colors.textLight}
                    />
                }}

                getComponent={() => require('./StackNavigator').HomeStack}
            />
            <Tab.Screen
                name='Todo'
                options={{
                    ...commonOptions,
                    tabBarIcon: ({ focused }) => <Icon
                        source={focused ? Icons.settingActive : Icons.settingInactive}
                        size={focused ? 30 : 25}
                        tintColor={focused ? colors.theme : colors.textLight}
                    />
                }}

                getComponent={() => require('./StackNavigator').HomeStack}
            />
        </Tab.Navigator>
    )
}

export default BottomTabNavigator