import { ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import ViewContainer from '../../Components/HOC/ViewContainer'
import ScrollContainer from '../../Components/HOC/ScrollContainer'
import Card from '../../Components/UI/Card'
import FocusAwareStatusBar from '../../Components/UI/FocusAwareStatusBar'
import colors from '../../Constants/colors'
import Header from '../../Components/UI/Header'
import Icons from '../../Constants/Icons'
import Icon from '../../Components/UI/Icon'
import Typography from '../../Components/UI/Typography'
import Press from '../../Components/HOC/Press'
import { FULL_WIDTH, STANDARD_WIDTH } from '../../Constants/layout'
import { LineChart } from 'react-native-chart-kit'
import ColoredView from '../../Components/HOC/ColoredView'

const PM_ButtonsData = [
    {
        id: 0,
        name: 'Projects',
        val: '150',
        icon: Icons.projects,
        route: 'Projects'
    },
    {
        id: 1,
        name: 'Clients',
        val: '75',
        icon: Icons.client,
        route: 'Clients'
    },
    {
        id: 2,
        name: 'Ongoing',
        val: '50',
        icon: Icons.ongoing,
        route: 'Ongoing'
    },
    {
        id: 3,
        name: 'Done',
        val: '150',
        icon: Icons.done,
        route: 'Done'
    },
]

const Dashboard = ({ navigation }) => {
    return (
        <ViewContainer>
            <FocusAwareStatusBar backgroundColor={colors.white} barStyle='dark-content' />
            <Header
                headerLeft={
                    <Icon
                        source={Icons.menu}
                        touchable
                        onPress={() => navigation.openDrawer()}
                        size={25}
                    />
                }
                headerRight={
                    <Icon
                        source={Icons.bell}
                        size={25}
                    />
                }
            />
            <ScrollContainer>
                <Card>
                    <Typography
                        size={13}
                        color={colors.textGray}
                    >
                        Good afternoon!
                    </Typography>
                    <Typography
                        size={23}
                        style={{
                            fontWeight: 'bold'
                        }}
                        color={colors.black}
                    >
                        Zevanya Casey
                    </Typography>
                </Card>
                <Card style={{ backgroundColor: colors.inputBG, ...styles.newTask }}>
                    <View>
                        <Typography
                            size={15}
                            color={colors.black}
                            style={{
                                fontWeight: 'bold',
                                marginBottom: 5
                            }}
                        >
                            Create new task
                        </Typography>
                        <Typography
                            size={12}
                            color={colors.textLight}>
                            You can create new task here
                        </Typography>
                    </View>
                    <Press style={styles.addTask}>
                        <Icon
                            source={Icons.plus}
                            size={20}
                            tintColor={colors.white}
                        />
                    </Press>
                </Card>
                <Card style={styles.PM_container}>
                    {
                        PM_ButtonsData.map((item, i) => {
                            return (
                                <Press style={styles.PM_Buttons} key={i}>
                                    <Icon
                                        source={item.icon}
                                        size={35}
                                    />
                                    <View>
                                        <Typography
                                            size={10}
                                            color={colors.textLight}>
                                            {item.name}
                                        </Typography>
                                        <Typography
                                            size={18}
                                            color={colors.black}
                                            style={{
                                                fontWeight: 'bold',
                                                marginBottom: 5
                                            }}>
                                            {item.val}
                                        </Typography>
                                    </View>
                                </Press>
                            )
                        })
                    }

                </Card>
                <Card style={[styles.graph, { marginVertical: 0 }]}>
                    <TouchableOpacity
                        activeOpacity={0.95}
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            marginTop: 7,
                            padding: 10,
                        }}>
                        <View>
                            <Typography
                                size={25}
                                color={colors.white}
                                style={{ fontWeight: 'bold', marginBottom: 5 }}
                            >
                                Activity
                            </Typography>
                            <Typography>
                                <Typography size={15} style={{ fontWeight: 'bold' }} color={colors.white}>30</Typography>
                                <Typography size={12} color={colors.white}>hours,</Typography>
                                <Typography size={15} style={{ fontWeight: 'bold' }} color={colors.white}>50</Typography>
                                <Typography size={12} color={colors.white}>min on this week</Typography>
                            </Typography>
                        </View>
                        <Icon
                            source={Icons.right_arrow}
                            size={18}
                            tintColor={colors.white}
                        />
                    </TouchableOpacity>
                    <View style={{ height: 250, justifyContent: "flex-end", backgroundColor: 'transparent' }}>
                        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                            <LineChart
                                data={{
                                    labels: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
                                    datasets: [
                                        {
                                            data: [
                                                Math.random() * 100,
                                                Math.random() * 100,
                                                Math.random() * 100,
                                                Math.random() * 100,
                                                Math.random() * 100,
                                                Math.random() * 100,
                                                Math.random() * 100
                                            ]
                                        }
                                    ]
                                }}
                                width={STANDARD_WIDTH} // from react-native
                                height={250}
                                yAxisLabel="$"
                                yAxisSuffix="k"
                                yAxisInterval={1} // optional, defaults to 1
                                chartConfig={{
                                    propsForVerticalLabels: { fontSize: 9, },
                                    backgroundColor: colors.theme,
                                    backgroundGradientFrom: colors.theme,
                                    backgroundGradientTo: colors.theme,
                                    decimalPlaces: 2, // optional, defaults to 2dp
                                    color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                                    labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,

                                    style: {
                                        borderRadius: 20,
                                        flex: 1,
                                        width: STANDARD_WIDTH,
                                    },
                                    propsForDots: {
                                        r: "6",
                                        strokeWidth: "2",
                                        stroke: colors.theme,

                                    }
                                }}
                                bezier
                                style={{
                                    borderRadius: 16,
                                    width: STANDARD_WIDTH
                                }}
                            />
                        </ScrollView>
                    </View>
                </Card>
            </ScrollContainer>
        </ViewContainer>
    )
}

export default Dashboard

const styles = StyleSheet.create({
    newTask: {
        borderRadius: 17,
        flexDirection: 'row',
        padding: 15,
        justifyContent: "space-between",
        alignItems: "center",
        marginVertical: 0
    },
    addTask: {
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: colors.theme,
        justifyContent: 'center',
        alignItems: 'center'
    },
    // PM_ ==> Projects Management
    PM_container: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between'
    },
    PM_Buttons: {
        width: STANDARD_WIDTH * 0.48,
        height: (STANDARD_WIDTH * 0.48) / 2,
        elevation: 1,
        backgroundColor: colors.white,
        marginBottom: STANDARD_WIDTH * 0.04,
        borderRadius: 15,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    graph: {
        width: STANDARD_WIDTH,
        height: STANDARD_WIDTH,
        backgroundColor: colors.theme,
        marginBottom: 10,
        borderRadius: 25,
        justifyContent: 'space-between'
    }
})