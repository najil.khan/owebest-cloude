import React from 'react'
import { Button, StyleSheet, Text, View } from 'react-native'
import ViewContainer from '../../Components/HOC/ViewContainer'
import Typography from '../../Components/UI/Typography'
import { useAuth } from '../../Context/UserContext'

const Login = () => {
    const { updateIsAuth } = useAuth()
    return (
        <ViewContainer>
            <Typography>
                Login
            </Typography>
            <Button title='Login' onPress={() => updateIsAuth(true)} />
        </ViewContainer>
    )
}

export default Login

const styles = StyleSheet.create({})